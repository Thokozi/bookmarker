//listen for form submit
document.getElementById('myForm').addEventListener('submit', saveBookmark);

//save Bookmark
function saveBookmark(e){
    //get form values
    var siteName = document.getElementById('siteName').value;
    var siteUrl = document.getElementById('siteURL').value;

    var bookmark = {
        name: siteName,
        url: siteUrl
    };

    //local Storage test
    /**localStorage.setItem('test', 'hello');

    localStorage.getItem('test');*/
  //Test if bookmarks is null
    if (localStorage.getItem('bookmarks') === null){
        //init array
        var bookmarks = [];

        //Add to array
         bookmarks.push(bookmark);
         //set localStorage
        localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    }else{
        // Get bookmarks form localStorage
        var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
        //Add bookmark to array
        bookmarks.push(bookmark);
        //Re-set back to localStorage
        localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    }

    //Prevent form from submitting
    e.preventDefault();
}

function fetchBookmarks(){
    //Get bookmarks from localStorage
    var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));

    //Get output ID
    var  bookmarksResults = document.getElementById('bookmarksResults');

    //Build output
    bookmarksResults.innerHTML = '';
    for (var i = 0; i < bookmarks.length; i++){
        var name = bookmarks[i].name;
        var url = bookmarks[i].url;
    }
}